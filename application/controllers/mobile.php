<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile extends CI_Controller {

    public function list_dokter($id_spesialis)
	{
		$query = $this->mobile_m->GetListDokter($id_spesialis);
		echo json_encode(array('result'=>$query));
	}

	public function insert_antrian()
	{
		$nama = $_POST['nama'];
		$alamat = $_POST['alamat'];
		$id_dokter = $_POST['id_dokter'];

		$data_insert = array(
			'nama' => $nama,
			'alamat' => $alamat,
			'id_dokter' => $id_dokter,
			'tanggal' => date('Y-m-d H:i:s')
			);

		$query = $this->mobile_m->InsertData('antrian',$data_insert);

		//get nomor antrian
		$thisday = date('Y-m-d').'%';
		$nomorantrian;
		$query = $this->mobile_m->GetJumlahAntrianHariIni($id_dokter,$thisday);
		foreach ($query as $data) {
			$nomorantrian = $data['jumlah_antrian'];
		}

		if ($query >= 1) {
			echo $nomorantrian;
		} else {
			echo "Could Not Add Data";
		}
	}

	public function cek_akun()
	{
		$usernm = $_POST['usernm'];
		$passwd = $_POST['passwd'];

		$id_dokter = 0;
		$query = $this->mobile_m->GetDataAkun($usernm,$passwd);
		foreach ($query as $data) {
			$id_dokter = $data['id_dokter'];
		}

		if ($query >= 1) {
			echo $id_dokter;
		} else {
			echo "Could Not Get Data";
		}
	}

	public function get_dokter($id_dokter)
	{
		$array_tanggal[] = array('tanggal'=>date('D, d M Y'));

		//get nomor antrian
		$thisday = date('Y-m-d').'%';
		//$nomorantrian;
		$query_jumlah_antrian = $this->mobile_m->GetJumlahAntrianHariIni($id_dokter,$thisday);
		/* foreach ($query as $data) {
			$nomorantrian = $data['jumlah_antrian'];
		} */
		$query_list_antrian = $this->mobile_m->GetDataListAntrian($id_dokter,$thisday);

		$query_get_dokter = $this->mobile_m->GetDataDokter($id_dokter);
		echo json_encode(array('result_dokter'=>$query_get_dokter,
								'result_jumlah_antrian'=>$query_jumlah_antrian,
								'result_tanggal'=>$array_tanggal,
								'result_list_antrian'=>$query_list_antrian
								));
	}
}
