/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.19-MariaDB : Database - dokter_spesialis
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `akun` */

DROP TABLE IF EXISTS `akun`;

CREATE TABLE `akun` (
  `id_akun` int(10) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(20) NOT NULL,
  `id_dokter` int(10) DEFAULT NULL,
  `usernm` varchar(50) DEFAULT NULL,
  `passwd` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_akun`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `akun` */

insert  into `akun`(`id_akun`,`jabatan`,`id_dokter`,`usernm`,`passwd`) values (1,'dokter',1,'ahmad','123456'),(2,'dokter',2,'zaky','123456'),(3,'dokter',3,'maula','123456'),(4,'dokter',4,'maulana','123456');

/*Table structure for table `antrian` */

DROP TABLE IF EXISTS `antrian`;

CREATE TABLE `antrian` (
  `id_antrian` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` text,
  `id_dokter` int(10) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`id_antrian`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `antrian` */

insert  into `antrian`(`id_antrian`,`nama`,`alamat`,`id_dokter`,`tanggal`) values (1,'pasien ahmad','bandar kidul',1,'2018-12-04 17:08:27'),(2,'pasien ahmad 2','bandar lor',1,'2018-12-05 17:09:51'),(3,'pasien ahmad 3','jamsaren',1,'2018-12-05 23:21:46'),(4,'pasien ahmad 5','pesantreb',1,'2018-12-05 09:20:54'),(5,'pasien ahmad 6','pesantren',1,'2018-12-05 09:21:24'),(6,'pasien ahmad 7','pesantren',1,'2018-12-05 09:24:36'),(7,'pasien ahmad 8','pesantren',1,'2018-12-05 09:24:45'),(8,'pasien 1','bandar',1,'2018-12-05 09:30:11'),(9,'pasien 2','bandar',1,'2018-12-05 09:31:28'),(10,'zaky 1','',5,'2018-12-05 09:39:13'),(11,'zaky 2','',5,'2018-12-05 09:40:47'),(12,'zaky 3','',5,'2018-12-05 09:42:30'),(13,'zaky 4','bandar',5,'2018-12-05 09:42:54'),(14,'Pasien 1','Lirboyo',9,'2018-12-07 20:49:10'),(15,'Pasien 2','Lirboyo 2',9,'2018-12-07 20:50:04'),(16,'doni','bandar',1,'2018-12-11 22:43:21'),(17,'pasien 1','bandar',1,'2018-12-12 09:56:08');

/*Table structure for table `dokter` */

DROP TABLE IF EXISTS `dokter`;

CREATE TABLE `dokter` (
  `id_dokter` int(10) NOT NULL AUTO_INCREMENT,
  `id_spesialis` int(10) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` text,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  PRIMARY KEY (`id_dokter`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `dokter` */

insert  into `dokter`(`id_dokter`,`id_spesialis`,`nama`,`alamat`,`latitude`,`longitude`) values (1,1,'Ahmad','Bandar Kidul','-7.811012','112.009449'),(2,2,'Zaky','Sidomulyo','-7.818283','112.009277'),(3,3,'Maula','Bandar Lor','-7.824277','112.011079'),(4,4,'Maulana','Mojoroto','-7.810162','112.004642'),(5,1,'Zaky2','Lirboyo','-7.817602','112.006015'),(6,2,'Maula2','Pesantren','-7.805060','112.004385'),(7,3,'Maulana2','Tinalan','-7.817602','112.006015'),(8,4,'Ahmad2','Balowerti','-7.810162','112.004642'),(9,1,'Joko','Lirboyo','-7.824277','112.011079'),(10,1,'Yoga','Pakunden','-7.818283','112.009277');

/*Table structure for table `spesialis` */

DROP TABLE IF EXISTS `spesialis`;

CREATE TABLE `spesialis` (
  `id_spesialis` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_spesialis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `spesialis` */

insert  into `spesialis`(`id_spesialis`,`nama`) values (1,'Dokter Spesialis Gigi'),(2,'Dokter Spesialis Anak'),(3,'Dokter Spesialis Mata'),(4,'Dokter Spesialis Kulit');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
