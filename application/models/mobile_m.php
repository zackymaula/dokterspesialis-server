<?php
class Mobile_m extends CI_Model{

    public function GetListDokter($where="")
    {
        $data = $this->db->query("SELECT d.id_dokter, s.nama AS spesialis, d.nama, d.alamat, d.latitude, d.longitude
                                    FROM dokter AS d, spesialis AS s
                                    WHERE d.id_spesialis = s.id_spesialis
                                    AND d.id_spesialis = '".$where."'");
        return $data->result_array();
    }

    public function InsertData($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
    }
    
    public function GetJumlahAntrianHariIni($id_dokter="",$tanggal="")
    {
        $data = $this->db->query("SELECT COUNT(id_antrian) AS jumlah_antrian
                                    FROM antrian
                                    WHERE id_dokter = '".$id_dokter."'
                                    AND tanggal LIKE '".$tanggal."'");
        return $data->result_array();
    }

    public function GetDataAkun($usernm="",$passwd="")
    {
        $data = $this->db->query("SELECT * FROM akun
                                    WHERE usernm = '".$usernm."'
                                    AND passwd = '".$passwd."'");
        return $data->result_array();
    }

    public function GetDataDokter($and="")
    {
        $data = $this->db->query("SELECT d.id_dokter, d.nama, d.alamat, s.nama AS spesialis
                                    FROM dokter AS d, spesialis AS s
                                    WHERE d.id_spesialis = s.id_spesialis
                                    AND d.id_dokter = '".$and."'");
        return $data->result_array();
    }

    public function GetDataListAntrian($id_dokter="",$tanggal="")
    {
        $data = $this->db->query("SELECT * FROM antrian
                                    WHERE id_dokter = '".$id_dokter."'
                                    AND tanggal LIKE '".$tanggal."'
                                    ORDER BY tanggal ASC");
        return $data->result_array();
    }
}
